﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace AarhusWebDevelopersNetwork.ViewModels
{
    public class Message
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string MessageText { get; set; }
    }
}