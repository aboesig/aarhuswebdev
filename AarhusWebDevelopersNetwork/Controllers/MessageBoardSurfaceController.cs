﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AarhusWebDevelopersNetwork.ViewModels;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;

namespace AarhusWebDevelopersNetwork.Controllers
{
    public class MessageBoardSurfaceController : SurfaceController
    {
        // GET: MessageFormSurface
        public ActionResult Index()
        {
            return PartialView("MessageForm", new Message());
        }

        private void CreateAndSaveMessage(Message model)
        {
            IContent message = Services.ContentService.CreateContent(model.Name, CurrentPage.Id, "Message");
            // assign values
            message.SetValue("messageName", model.Name);
            message.SetValue("messageMessage", model.MessageText);

            Services.ContentService.SaveAndPublishWithStatus(message);
        }

        [HttpPost]
        public ActionResult HandleFormSubmit(Message model)
        {
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            CreateAndSaveMessage(model);

            return RedirectToCurrentUmbracoPage();
        }

    }
}